# D15 display-deltaE

## Purpose
Color arrangement tests, such as the D15 (Farnsworth) and D15DS (Lanthony) are commonly utilized for screening and diagnosing color vision deficiency (CVD).

Ideally, these tests should be administered in a standard environment. While current display technology may enable color vision testing with digital displays as well and this has been already demonstrated both with pseudoisochromatic images and with color arrangement tests, these tests were carried out using calibrated or already color accurate displays.

Such data on generic displays is scarce, and the purpose of this project is to facilitate a more generalized approach to display choice for color vision testing.

## Modeling approach
- The script utilizes ICC color profiles published by [NotebookCheck](https://www.notebookcheck.net/) to approximate the expected diagnostic performance on a multitude of displays
- This is achieved by reversing and optimizing the equation used to apply ICC color profiles to individual RGB pixels, for details on the equation see the [white paper](https://www.color.org/wpaper1.xalter). 
- CVDs are modeled using [Colorspacious](https://colorspacious.readthedocs.io/en/latest/tutorial.html#simulating-colorblindness)
- Color arrangements are modeled using the algorithm proposed by [Hovis and Almustanyir (2022)](https://cormusa.org/wp-content/uploads/2022/01/CORM2021-Color-Hovis.pdf).
- Color arrangements are scored using the [Vyngris and King-Smith (1988)](https://pubmed.ncbi.nlm.nih.gov/3257208/) and the [Foutch et al. (2011)](https://www.researchgate.net/profile/Vasudevan-Lakshminarayanan/publication/233249365_A_new_quantitative_technique_for_grading_Farnsworth_D-15_color_panel_tests/links/57ef04e208ae91deaa510c38/A-new-quantitative-technique-for-grading-Farnsworth-D-15-color-panel-tests.pdf?origin=publication_detail) algorithms.

## Usage
Python3 and pip are required. Clone this repository, install the dependencies, and run `main.py`.
```
git clone https://gitlab.com/ThomasHastings/d15-display-deltae.git

python3 -m pip install colorspacious colormath matplotlib scipy

cd d15-display-deltae/src/

python3 main.py
```


## Acknowledgements
- [NotebookCheck](https://www.notebookcheck.net/) – ICC profiles
- [icc_dump](https://github.com/mplough/icc_dump) by [https://github.com/mplough](mplough) – converting ICC profiles for calculations
- [picture_to_cie_diagram](https://gist.github.com/Rachel030219/1bdf6c6eb63115a9a61eb27618ecb579) by [Rachel](https://rachelt.one/en/) – reference implementation for ICC profile utilization


## Disclaimer
This software is provided as is under the GPLv3 license, without any warranties. No user-related data is logged or processed.

## Contact
You can reach the developer of this project at: thomas.hastings@tutanota.com