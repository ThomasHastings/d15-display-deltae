def vyngris(color_arrangement, color_values_list_luv, R2):
    from math import sin as SIN
    from math import cos as COS
    from math import atan as ATN
    from math import sqrt as SQR
    
    # line 70
    u_sum = 0
    v_sum = 0
    u_list = []
    v_list = []
    
    # line 80 SU and SV creation
    for N in range(len(color_values_list_luv)):
        u_list.append(color_values_list_luv[N][0])
        v_list.append(color_values_list_luv[N][1])
        u_sum = u_sum + u_list[N]
        v_sum = v_sum + v_list[N]
    
    if round(u_sum,2) != 26.86 or round(v_sum,2) != -38.69:
        #print('DATA error, recheck line 600 DATA entry.')
        pass
    
    # lines 100-210 entry of cap color_arrangement, in this case it is given from the argument
    C = color_arrangement[:]

    # line 212: C(0) is 0 for D15-DS, here we include 0 in the argument
    #C.insert(0,0)
    
    # lines 215-260: calculate sums of squares and cross products
    U2 = 0
    V2 = 0
    UV = 0
    
    # color difference vectors
    for N in range(1,len(color_values_list_luv)):
        DV = v_list[C[N]] - v_list[C[N-1]]
        DU = u_list[C[N]] - u_list[C[N-1]]
        U2 = U2+DU**2
        V2 = V2+DV**2
        UV = UV+DU*DV
    
    # angle
    D = U2-V2
    if D == 0:
        A0 = 0.7854
    else:
        A0 = ATN(2*UV/D)/2
    
    # major moment
    I0 = U2*SIN(A0)**2+V2*COS(A0)**2-2*UV*SIN(A0)*COS(A0)
    
    # perpendicular angle
    if A0 < 0:
        A1 = A0+1.5708
    else:
        A1 = A0-1.5708
    
    # minor moment
    I1 = U2*SIN(A1)**2+V2*COS(A1)**2-2*UV*SIN(A1)*COS(A1)
    
    # check that major moment is greater than minor and swap if needed
    if I0 < I1:
        P = A0
        A0 = A1
        A1 = P
        P = I0
        I0 = I1
        I1 = P
    
    # radii and total error
    R0 = SQR(I0/(len(color_values_list_luv)-1))
    R1 = SQR(I1/(len(color_values_list_luv)-1))
    R = SQR(R0**2+R1**2)
    angle = round(57.3*A1,3)
    maj_rad = round(R0, 3)
    min_rad = round(R1, 3)
    tot_err = round(R, 3)
    s_index = round(R0/R1, 3)
    c_index = round(R0/R2, 3)
    # print result
    #print("ANGLE    MAJ RAD   MIN RAD    TOT ERR   S-INDEX    C-INDEX")
    #print(round(57.3*A1,2), "   ", round(R0, 2), "    ", round(R1, 2), "      ", round(R, 2), "    ", round(R0/R1, 2), "    ", round(R0/R2, 6))
    
    # interpretation
    if s_index > 1.65 or c_index > 1.78:
        if -2 <= angle < 29:
            dg = 'protan'
        elif -25 < angle < -2:
            dg = 'deutan'
        elif -90 < angle < -65:
            dg = 'tritan'
        else:
            dg = 'undetermined'
    else:
        dg = 'normal'
        
    # side quest: based on the deltaE=0 set of modeled cvds, anomaly-anopy cutoffs for c_ and s_indices could be calculated
    
    result_dict = {'angle': angle, 'maj_rad': maj_rad, 'min_rad': min_rad, 'tot_err': tot_err, 's_index': s_index, 'c_index': c_index, 'diagnosis': dg}
    return result_dict


def foutch(arrangement, color_values_list_luv):
    from math import atan
    from math import degrees
    from math import tan
    u_list = []
    v_list = []
    for color in color_values_list_luv:
        u_list.append(color[0])
        v_list.append(color[1])
    
    # find positions where errors occured
    error_pairs = []
    for i in range(1,len(arrangement)):
        if abs(arrangement[i-1]-arrangement[i]) != 1:
            error_pair = (arrangement[i-1], arrangement[i])
            error_pairs.append(error_pair)
    
    # define best fit angle and error_sum
    # angle represents error type
    du_dv_list = []
    magnitude_sum = 0
    du_dv_sum = 0
    du_sq_sum = 0
    for pair in error_pairs:
        du = u_list[pair[0]] - u_list[pair[1]]
        dv = v_list[pair[0]] - v_list[pair[1]]
        du_dv_list.append((du, dv))
        du_dv_sum = du_dv_sum + du*dv
        du_sq_sum = du_sq_sum + du*du
        magnitude = (du ** 2 + dv ** 2) ** 0.5
        magnitude_sum = magnitude_sum + magnitude
    
    try:
        phi = degrees(atan(du_dv_sum / du_sq_sum))
        
        while phi > 90:
            phi = phi-180
            
        while phi < -90:
            phi = phi+180
        
        # calculate residuals
        # the sum of residuals represents selectivity
        residuals = []
        deltasq = 0
        if phi > 45:
            for du_dv in du_dv_list:
                actual_du = du_dv[0]
                calculated_du = tan(atan(du_dv_sum / du_sq_sum))*du_dv[1]
                residual = actual_du - calculated_du
                residuals.append(residual)
                deltasq = deltasq + (residual*residual / ( len(du_dv_list) - 1 ))
           
        else:
            for du_dv in du_dv_list:
                actual_dv = du_dv[1]
                calculated_dv = tan(atan(du_dv_sum / du_sq_sum))*du_dv[0]
                residual = actual_dv - calculated_dv
                residuals.append(residual)
                deltasq = deltasq + (residual*residual / ( len(du_dv_list) - 1 ))
             
        
        # interpretation
        if magnitude_sum > 83:
            if -30 < phi <= 0:
                dg = 'deutan'
            elif 0 < phi  <= 30:
                dg = 'protan'
            elif abs(phi) > 50:
                dg = 'tritan'
            else:
                dg = 'undetermined' # "others"
        else:
            dg = 'normal'
        
        return {'angle': round(phi,2), 'selectivity': round(deltasq,2), 'tot_err': round(magnitude_sum,2), 'diagnosis': dg}
    
    except ZeroDivisionError:
        return {'angle': 0, 'selectivity': 0, 'tot_err': round(magnitude_sum,2), 'diagnosis': 'normal'}
    
    
def test_foutch(color_arrangement=None):
    if color_arrangement == None:
        color_arrangement=[0,1,2,3,4,5,6,7,15,8,14,9,10,13,12,11]
   #color_arrangement=[0,15,2,3,4,5,6,7,8,9,10,12,11,13,14,1]
    
    
    d15_colors = [
            (-21.54,-38.39),
            (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
            (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
            (-2.72,28.13), (14.84,31.13), (23.87,26.35),
            (31.28,14.76), (31.42,6.99), (29.79,0.10),
            (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]
    print(foutch(color_arrangement, d15_colors))
