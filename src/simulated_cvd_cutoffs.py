from pickle_tools import *
from generate_display_color_values import *
from simulate_cvd import *
from simulate_color_arrangement import *
from scoring_algorithms import *

#### COLOR GENERATION ###
# takes:   original color values
# takes:   deltaE targets
# work:    introduce loss of color accuracy
# method:  deltaE radius + angle
# returns: modified_color_values_list_luv

# canonical color_values for the D15 and D15DS panels from Vyingris and King-Smith 1988
calculate_expected_diagnoses():
    d15_colors = [
                (-21.54,-38.39),
                (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
                (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
                (-2.72,28.13), (14.84,31.13), (23.87,26.35),
                (31.28,14.76), (31.42,6.99), (29.79,0.10),
                (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]

    d15ds_colors = [
                (-4.77,-16.63),
                (-8.63,-14.65), (-12.08,-11.94), (-12.86,-6.74),
                (-12.26,-2.67), (-11.18,2.01),   (-7.02,9.12),
                (1.30,15.78),   (9.90,16.46),    (15.03,12.05),
                (15.48,2.56),   (14.76,-2.24),   (13.56,-5.04),
                (11.06,-9.17),  (8.95,-12.39),   (5.62,-15.20)]


    expected_diagnoses = {'vks': {'d15':   {'protan': {},
                                            'deutan': {},
                                            'tritan': {}},
                                  'd15ds': {'protan': {},
                                            'deutan': {},
                                            'tritan': {}} 
                                      },
                          'fsl': {'d15':  {'protan': {},
                                           'deutan': {},
                                           'tritan': {}},
                                  'd15ds':{'protan': {},
                                           'deutan': {},
                                           'tritan': {}}}}

    for original_color_values_luv in [d15_colors, d15ds_colors]:
        # generate filenames and set R2
        if original_color_values_luv == d15_colors:
            R2 = 9.234669 # for D15
            panel_name = "Munsell's D15"
            panel_shortname = 'd15'
            
        else:
            R2 = 5.121259 # for D15-DS
            panel_name = "Lanthony's desaturated D15"
            panel_shortname = 'd15ds'

        # resolution represents step values (in %) for the simulated cvd severity
        cvd_color_values_filename = '_baseline_cvd_colors_'+panel_shortname+'.pickle'
        cvd_arrangements_filename = '_baseline_cvd_arrangements_'+panel_shortname+'.pickle'

        original_color_values_srgb = luv_convert_list(original_color_values_luv)[1]
        original_color_values_list = [{'srgb': original_color_values_srgb, 'filename': 'canonical_'+panel_shortname}]
        try:
            cvd_color_values_list = pickle_load(cvd_color_values_filename)
        except:
            cvd_color_values_list = simulate_cvd_color_values(original_color_values_list)
            pickle_dump(cvd_color_values_filename, cvd_color_values_list)

        try:
            cvd_arrangements = pickle_load(cvd_arrangements_filename)
        except:
            cvd_arrangements = arrange_color_values_deltaE(cvd_color_values_list)
            pickle_dump(cvd_arrangements_filename, cvd_arrangements)

        for arrangement in cvd_arrangements:
            resv = vyngris(arrangement['arrangement'], original_color_values_luv, R2)
            resf = foutch(arrangement['arrangement'], original_color_values_luv)
            expected_diagnoses['vks'][panel_shortname][arrangement['diagnosis']][str(int(arrangement['severity']))] = resv['diagnosis']
            expected_diagnoses['fsl'][panel_shortname][arrangement['diagnosis']][str(int(arrangement['severity']))] = resf['diagnosis']

    return expected_diagnoses
