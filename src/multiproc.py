import multiprocessing
from pickle_tools import *

def parallelize(target_function, args, threads):
    target_list = args[0]
    
    # divide the target_list for the number of threads available
    print('Dividing target list to', threads, 'sublists...')
        
    length = len(target_list)
    sublengths = int(length / threads)
    
    target_sublists = []
    for thread in range(threads):
        target_sublists.append([])
    
    target_sublist = 0
    thread = 0
    for entry in target_list:
        target_sublists[thread].append(entry)
        thread += 1
        if thread > threads-1:
            thread = 0
    
    # run parallelized calculation
    print('Starting parallelized work...')
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    jobs = []

    for procnum in range(threads):
        job_args = [target_sublists[procnum]]
        if len(args) > 1:
            for i in range(1,len(args)):
                job_args.append(args[i])
        job_args.append(return_dict)
        job_args.append(procnum)

        p = multiprocessing.Process(target=target_function, args=job_args)
        jobs.append(p)
        p.start()
        
    for proc in jobs:
        proc.join()
    
    result = []
    for sublist in return_dict.values():
        for entry in sublist:
            result.append(entry)
    
    pickle_dump('last_parallel_result.pickle', result)
    print(len(result), 'from', len(target_list))
    
    return result


from simulate_display_color_icc import *
from prepare_icc_profiles import *

if __name__ == "__main__":
    d15_colors = [
                (-21.54,-38.39),
                (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
                (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
                (-2.72,28.13), (14.84,31.13), (23.87,26.35),
                (31.28,14.76), (31.42,6.99), (29.79,0.10),
                (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]

    d15ds_colors = [
                (-4.77,-16.63),
                (-8.63,-14.65), (-12.08,-11.94), (-12.86,-6.74),
                (-12.26,-2.67), (-11.18,2.01),   (-7.02,9.12),
                (1.30,15.78),   (9.90,16.46),    (15.03,12.05),
                (15.48,2.56),   (14.76,-2.24),   (13.56,-5.04),
                (11.06,-9.17),  (8.95,-12.39),   (5.62,-15.20)]

    icc_profiles = prepare_icc_profiles('icc_json')
    
    test_output = parallelize(simulate_display_color_values,[icc_profiles, d15_colors],4)
