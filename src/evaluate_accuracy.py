from scoring_algorithms import *

def get_predictive_value(original_dg, new_dg, s_index=None):
    dyschr = ['protan', 'deutan', 'tritan', 'undetermined']   
    new_predictions = {'overall': None, 'protan': None, 'deutan': None, 'tritan': None}
    
    if original_dg == 'normal':
        if new_dg == 'normal':
            new_predictions['overall'] = 'tn'
            for anomaly in dyschr:
                new_predictions[anomaly] = 'tn'
        else: # new dg introduced false anomaly
            #if new_dg == 'undetermined' and s_index < 2: # clinically not significant, unspecified change
            #    new_predictions['overall'] = 'tn'
            #else:
                new_predictions['overall'] = 'fp'
                for anomaly in dyschr:
                    if str(anomaly) == str(new_dg):
                        new_predictions[anomaly] = 'fp'
                    else:
                        new_predictions[anomaly] = 'NA'
            
    else: # original dg showed anomaly
        if new_dg == 'normal': # anomaly missed
            new_predictions['overall'] = 'fn'
            #print(arrangement,mod_cap_colors)
            for anomaly in dyschr:
                new_predictions[anomaly] = 'fn'
        else: # anomaly found
            new_predictions['overall'] = 'tp'
            for anomaly in dyschr:
                if str(anomaly) == str(new_dg):
                    if new_dg == original_dg:
                        new_predictions[anomaly] = 'tp'
                    else:
                        new_predictions[anomaly] = 'fp'
                else:
                    new_predictions[anomaly] = 'NA'
        
    return new_predictions

def evaluate_cvd_accuracy(arrangements_list, color_values_luv, R2, expected_diagnoses, panel_shortname):
    
    dyschr = ['protan', 'deutan', 'tritan', 'undetermined']
    accuracy_list = []
    for arrangement in arrangements_list:
        original_diagnosis_vks = expected_diagnoses['vks'][panel_shortname][arrangement['diagnosis']][str(int(arrangement['severity']))]
        original_diagnosis_fsl = expected_diagnoses['fsl'][panel_shortname][arrangement['diagnosis']][str(int(arrangement['severity']))]
        new_diagnosis_vks = vyngris(arrangement['arrangement'], color_values_luv, R2)['diagnosis']
        new_diagnosis_fsl = foutch(arrangement['arrangement'], color_values_luv)['diagnosis']
        
        prediction_v = get_predictive_value(original_diagnosis_vks, new_diagnosis_vks)
        prediction_f = get_predictive_value(original_diagnosis_fsl, new_diagnosis_fsl)
        new_entry = {'vyngris':        prediction_v['overall'],
                     'vyngris_protan': prediction_v['protan'],
                     'vyngris_deutan': prediction_v['deutan'],
                     'vyngris_tritan': prediction_v['tritan'],
                     'foutch':         prediction_f['overall'],
                     'foutch_protan':  prediction_f['protan'],
                     'foutch_deutan':  prediction_f['deutan'],
                     'foutch_tritan':  prediction_f['tritan'],
                      }
        
        #print(prediction['overall'], new_diagnosis, arrangement['diagnosis'], arrangement['severity'])
        
        for key in arrangement.keys():
            if key not in ['srgb', 'cam', 'luv', 'arrangement', 'plot_rad']:
                new_entry[key] = arrangement[key] 
        accuracy_list.append(new_entry)
        
    return accuracy_list
            
from pickle_tools import *


if __name__ == "__main__":
    expected_diagnoses = pickle_load('_expected_cvd_diagnoses.pickle')
    d15_colors = [
            (-21.54,-38.39),
            (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
            (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
            (-2.72,28.13), (14.84,31.13), (23.87,26.35),
            (31.28,14.76), (31.42,6.99), (29.79,0.10),
            (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]

    d15ds_colors = [
            (-4.77,-16.63),
            (-8.63,-14.65), (-12.08,-11.94), (-12.86,-6.74),
            (-12.26,-2.67), (-11.18,2.01),   (-7.02,9.12),
            (1.30,15.78),   (9.90,16.46),    (15.03,12.05),
            (15.48,2.56),   (14.76,-2.24),   (13.56,-5.04),
            (11.06,-9.17),  (8.95,-12.39),   (5.62,-15.20)]
    
    arrangements = pickle_load('deltaE_arranged_cvd_color_values_d15.pickle')
    panel_shortname = 'd15'
    R2 = 5.121259 # for D15
    testing_accuracy=evaluate_cvd_accuracy(arrangements, d15_colors, R2, expected_diagnoses, panel_shortname)
    pickle_dump('test_accuracy4.pickle', testing_accuracy)