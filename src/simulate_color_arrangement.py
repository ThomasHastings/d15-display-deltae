from colorspacious import deltaE as calculate_deltaE
from itertools import product
from conversions import *
from simulate_color_arrangement import *

# arg color_arrangements_list is the return_dict for parallelized work
def arrange_color_values_deltaE(color_values_list, return_dict=None, procnum=None):
    try:
        color_values_list[0]['srgb']
    except KeyError:
        print('sRGB colorspace needs to be present in color_values_list')
        exit(1)
    
    if return_dict == None:
        multiproc = False
    else:
        multiproc = True
    
    color_arrangements_list = []
    calculations_done = 0
    for color_values in color_values_list:
        new_color_arrangements = []
        # generate the ideal arrangement, choose next color based on deltaE
        ideal_arrangement = [0] # P cap is always the starting point
        base_color = color_values['srgb'][0] # P cap is always the starting point
        
        while True:
            deltaE = float('inf')
            for i in range(0,len(color_values['srgb'])):
                if i not in ideal_arrangement:
                    this_deltaE = calculate_deltaE(base_color, color_values['srgb'][i], input_space="sRGB1") # CAM02-UCS will be the default uniform space
                    if this_deltaE < deltaE:
                        deltaE = this_deltaE
                        next_color = color_values['srgb'][i]
                        next_number = i
            ideal_arrangement.append(next_number)
            base_color = next_color
            if len(ideal_arrangement) == len(color_values['srgb']):
                break
        
        new_entry = {'arrangement': ideal_arrangement}
        for key in color_values.keys():
            if key not in ['srgb', 'cam', 'luv']:
                new_entry[key] = color_values[key]
        new_color_arrangements.append(new_entry)
        
        # calculate additional possible arrangements based on dE
        detectable_dE = 1 # if dE=1, the two colors are the same for almost any observer, higher values might be debatable
        close_color_sets = []
        for i in range(1,len(ideal_arrangement)):
            close_colors_detected = False
            close_color_set = [ideal_arrangement[i]]
            base_color = color_values['srgb'][ideal_arrangement[i]]
            for j in range(1,len(ideal_arrangement)):
                this_deltaE = calculate_deltaE(base_color, color_values['srgb'][ideal_arrangement[j]], input_space="sRGB1")
                if this_deltaE <= detectable_dE and ideal_arrangement[j] not in close_color_set:
                    close_color_set.append(ideal_arrangement[j])
                    close_colors_detected = True
            if close_color_set not in close_color_sets:
                close_color_sets.append(close_color_set)
        
        if close_colors_detected:
            for arrangement in product(*close_color_sets):
                new_arrangement = [0]
                no_duplication = True
                for item in arrangement:
                    if item not in new_arrangement:
                        new_arrangement.append(item)
                    else:
                        no_duplication = False
                        break
                
                if no_duplication and new_arrangement != ideal_arrangement:
                    new_entry = {'arrangement': new_arrangement}
                    for key in color_values.keys():
                        if key not in ['srgb', 'cam', 'luv']:
                            new_entry[key] = color_values[key]
                    new_color_arrangements.append(new_entry)
                    
        calculations_done += 1
        print(calculations_done,'/',len(color_values_list), new_entry['diagnosis'], new_entry['severity'])        
        
        for entry in new_color_arrangements:
            color_arrangements_list.append(entry)
        
        plot = False
        if plot:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots()
            ax.set_xlim(-55,55)
            ax.set_ylim(-15,10)
            ax.text(0,8,'deltaE: '+str(color_values['deltaE_target'])+'; positions: '+color_values['positions']+'; diagnosis: '+color_values['diagnosis']+'; severity: '+str(color_values['severity']), horizontalalignment='center', verticalalignment='center')  
            for i in range(len(color_values['srgb'])): 
                ax.text(i*6.7-49.5,5, i, horizontalalignment='center', verticalalignment='center')
                ax.text(i*6.7-49.5,-14, ideal_arrangement[i], horizontalalignment='center', verticalalignment='center')
                ax.add_patch(plt.Circle((i*6.7-49.5,0), 3.2, color=color_values['srgb'][i]))
                ax.add_patch(plt.Circle((i*6.7-49.5,-8), 3.2, color=color_values['srgb'][ideal_arrangement[i]]))
            #plt.savefig(target_dir+'/deltaE-'+str(color_values['deltaE_target'])+'_'+color_values['positions']+'.png', dpi=500)
            plt.show()
            plt.close()
    
    if multiproc:
         return_dict[procnum] = color_arrangements_list
    else:
        return color_arrangements_list

from pickle_tools import *

if __name__ == '__main__':
    cvd_color_values_list = pickle_load('cvd_colors_d15.pickle')
    arrange_color_values_deltaE(cvd_color_values_list)
