import pickle
def pickle_load(filename):
    with open('pickles/'+filename, 'rb') as fp:
        data = pickle.load(fp)
        print(filename, 'loaded.')
    return data

def pickle_dump(filename, data):
    with open('pickles/'+filename, 'wb') as fp:
        pickle.dump(data, fp)
    print(filename, 'saved.')

import os
if not os.path.isdir('pickles'):
    os.mkdir('pickles')