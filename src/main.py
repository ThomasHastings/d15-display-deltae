from pickle_tools import *
from multiproc import parallelize
import yaml

# load config file
config = {'panel_shortname': 'd15ds',
          'threads':          6}

# define filenames
# display color files
display_color_values_filename = 'display_colors_'+config['panel_shortname']+'.pickle'
deltaE_arranged_display_color_values_filename = 'deltaE_arranged_display_color_values_'+config['panel_shortname']+'.pickle'
# simulated cvd files
expected_diagnoses_filename = '_expected_cvd_diagnoses.pickle'
cvd_color_values_filename = 'cvd_colors_'+config['panel_shortname']+'.pickle'
deltaE_arranged_cvd_color_values_filename = 'deltaE_arranged_cvd_color_values_'+config['panel_shortname']+'.pickle'
cvd_accuracy_filename = 'cvd_accuracy_'+config['panel_shortname']+'.pickle'

# canonical color_values for the D15 and D15DS panels from Vyingris and King-Smith 1988
d15_colors = [
            (-21.54,-38.39),
            (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
            (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
            (-2.72,28.13), (14.84,31.13), (23.87,26.35),
            (31.28,14.76), (31.42,6.99), (29.79,0.10),
            (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]

d15ds_colors = [
            (-4.77,-16.63),
            (-8.63,-14.65), (-12.08,-11.94), (-12.86,-6.74),
            (-12.26,-2.67), (-11.18,2.01),   (-7.02,9.12),
            (1.30,15.78),   (9.90,16.46),    (15.03,12.05),
            (15.48,2.56),   (14.76,-2.24),   (13.56,-5.04),
            (11.06,-9.17),  (8.95,-12.39),   (5.62,-15.20)]

# set R2 for the panel
if config['panel_shortname'] == 'd15':
    original_color_values_luv = d15_colors
    R2 = 9.234669 # for D15
    panel_name = "Munsell's D15"
    
else:
    original_color_values_luv = d15ds_colors
    R2 = 5.121259 # for D15-DS
    panel_name = "Lanthony's desaturated D15"

# begin
print('starting display deltaE calculations for', panel_name)

### DISPLAY COLOR SIMULATION ###
# takes:   original_color_values_luv
# takes:   icc profiles (from notebookcheck.net)
# note:    icc profiles contain information about a display's color reproduction
# note:    we utilize this information to model the likely response from that display
#              when a certain pixel color is requested
# work:    scipy_optimize_minimize each D15/D15DS color on the function defined
#              in the ICC Specification (https://www.color.org/wpaper1.xalter)
# returns: color values with modified luv colorspace positions corresponding to each display
from prepare_icc_profiles import *
from simulate_display_color_icc import *

try:
    display_color_values_list = pickle_load(display_color_values_filename)
except FileNotFoundError:
    print('running simulate_display_color_values()...')
    icc_profiles = pickle_load('_icc_profiles.pickle')
    display_color_values_list = parallelize(simulate_display_color_values, [icc_profiles, original_color_values_luv],config['threads'])
    pickle_dump(display_color_values_filename, display_color_values_list)
print(len(display_color_values_list), 'display simulations will be used.')

### CVD-BASED APPROACH ###
# CVD SIMULATION
# takes:   cvd resolution
# takes:   display_color_values_list
# work:    generate simulated cvd arrangements for each color_values
# method:  deltaE-based reorder
# returns: simulated_deltaE_arranged_cvd_color_values + diagnoses
from simulate_cvd import *
from simulate_color_arrangement import *
try:
    cvd_color_values_list = pickle_load(cvd_color_values_filename)
except FileNotFoundError:
    print('running simulate_cvd_color_values()...')
    cvd_color_values_list = simulate_cvd_color_values(display_color_values_list)
    pickle_dump(cvd_color_values_filename, cvd_color_values_list)

try:
    deltaE_arranged_cvd_color_values = pickle_load(deltaE_arranged_cvd_color_values_filename)
except FileNotFoundError:
    print('running arrange_color_values_deltaE() for cvd...')
    deltaE_arranged_cvd_color_values = parallelize(arrange_color_values_deltaE, [cvd_color_values_list], config['threads'])
    pickle_dump(deltaE_arranged_cvd_color_values_filename, deltaE_arranged_cvd_color_values)
    
# CVD EVALUATION
# takes:   simulated_deltaE_arranged_cvd_color_values
# work:    run vyngris
# method:  **modified arrangements** and original colors
# returns: original-color-based diagnoses for each simulated arrangement
from evaluate_accuracy import *
try:
    expected_diagnoses = pickle_load(expected_diagnoses_filename)
except:
    expected_diagnoses = calculate_expected_diagnoses()
    pickle_dump(expected_diagnoses_filename, expected_diagnoses)

try:
    cvd_accuracy = pickle_load(cvd_accuracy_filename)
except FileNotFoundError:
    print('running evaluate_cvd_accuracy()...')
    cvd_accuracy = evaluate_cvd_accuracy(deltaE_arranged_cvd_color_values, original_color_values_luv, R2, expected_diagnoses, config['panel_shortname'])
    pickle_dump(cvd_accuracy_filename, cvd_accuracy)
    
# STATISTICS
# takes:   display_color_values_list
# takes:   cvd_accuracy
# work:    compare the diagnoses with the modeled displays to the ones with the canonical colors
# returns: Bland-Altman plots for deltaE/deltaS and scatterplots on diagnostic value 
from statistical_evaluation import *
diagnostic_test_statistic(cvd_accuracy, display_color_values_list, config['panel_shortname'])