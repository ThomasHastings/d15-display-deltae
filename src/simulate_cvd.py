from colorspacious import cspace_convert
import numpy as np

def simulate_cvd_color_values(display_color_values_list):
    # generate cvd colorspaces
    cvd_colorspaces = []
    for cvd_type in ['protanomaly','deuteranomaly','tritanomaly']:
        for severity in range(0,101):
            cvd_colorspaces.append({'name':    'sRGB1+CVD',
                                    'cvd_type': cvd_type,
                                    'severity': int(severity)})
    
    cvd_diagnoses = {'protanomaly':   'protan',
                     'deuteranomaly': 'deutan',
                     'tritanomaly':   'tritan'}
    
    # transform *all* display colors to *all* cvd colorspaces
    cvd_color_values_list_srgb = []
    for color_values in display_color_values_list:
        srgb_values_np = np.asarray([color_values['srgb']])
        for cvd_colorspace in cvd_colorspaces:
            cvd_color_values_np = cspace_convert(srgb_values_np, cvd_colorspace, "sRGB1")
            cvd_color_values = cvd_color_values_np.tolist()[0]
            new_entry = {'diagnosis': cvd_diagnoses[cvd_colorspace['cvd_type']],
                         'severity': cvd_colorspace['severity'],
                         'srgb': cvd_color_values}
            for key in color_values.keys():
                if key not in ['srgb', 'cam', 'luv']:
                    new_entry[key] = color_values[key] 
            cvd_color_values_list_srgb.append(new_entry)
            
    plot = False
    if plot:
        import datetime
        import matplotlib.pyplot as plt
        target_dir = 'fig_'+str(datetime.datetime.now())
        os.mkdir(target_dir)
        
        for color_values in cvd_color_values_list_srgb:
            fig, ax = plt.subplots()
            ax.set_xlim(-55,55)
            ax.set_ylim(-5,10)
            ax.text(0,8,'deltaE: '+str(color_values['deltaE_target'])+'; positions: '+color_values['positions']+'; diagnosis: '+color_values['diagnosis']+'; severity: '+str(color_values['severity']), horizontalalignment='center', verticalalignment='center')  
            for i in range(len(color_values['srgb'])): 
                ax.text(i*6.7-49.5,5, i, horizontalalignment='center', verticalalignment='center')
                ax.add_patch(plt.Circle((i*6.7-49.5,0), 3.2, color=color_values['srgb'][i]))          
            #plt.savefig(target_dir+'/deltaE-'+str(color_values['deltaE_target'])+'_'+color_values['positions']+'.png', dpi=500)
            plt.show()
            plt.close()
    
    print(len(cvd_color_values_list_srgb),'CVD color_values generated.')
    return cvd_color_values_list_srgb
