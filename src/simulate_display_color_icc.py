import numpy as np
from colorspacious import cspace_convert
from conversions import *
from scipy.optimize import minimize
from colorspacious import deltaE as calculate_deltaE
from prepare_icc_profiles import *
from deltae import delta_e_2000 as calculate_deltaE2000

def find_srgb(guess_srgb, profile, target_srgb=None):
    Lr = np.sign(guess_srgb[0]) * (np.abs(guess_srgb[0])) ** (profile['RedTRC_gamma'])
    Lg = np.sign(guess_srgb[1]) * (np.abs(guess_srgb[1])) ** (profile['GreenTRC_gamma'])
    Lb = np.sign(guess_srgb[2]) * (np.abs(guess_srgb[2])) ** (profile['BlueTRC_gamma'])
                    
    L_matrix = np.matrix([[Lr], [Lg], [Lb]])
    
    connection_matrix = profile['colorantMatrix'] * L_matrix

    X=float(np.ndarray.item(connection_matrix[0][0]))
    Y=float(np.ndarray.item(connection_matrix[1][0]))
    Z=float(np.ndarray.item(connection_matrix[2][0]))
    
    # the returned value is PCSXYZ
    # should be tha same as CIEXYZ, but with the D50 standard illuminant
    # an extra step of conversion is needed to change this (many thanks to https://rachelt.one/en/)
    from colormath.color_objects import XYZColor, sRGBColor
    from colormath.color_conversions import convert_color
    
    PCSXYZ1 = XYZColor(X, Y, Z, observer='2',illuminant="D50")
    fitted_srgb = convert_color(PCSXYZ1, sRGBColor, target_illuminant="D65")
    fitted_srgb = fitted_srgb.get_value_tuple()
    if target_srgb is not None:
        try:
            deltaE2000_mode = True
            if deltaE2000_mode:
                target_Lab = cspace_convert(target_srgb, 'sRGB1', 'CIELab')
                fitted_Lab  = cspace_convert(fitted_srgb, 'sRGB1', 'CIELab')
                deltaE2000 = round(calculate_deltaE2000({'L': target_Lab[0], 'a': target_Lab[1], 'b': target_Lab[2]},
                                                        {'L': fitted_Lab[0], 'a': fitted_Lab[1], 'b': fitted_Lab[2]}),2)
                return deltaE2000
            else:
                return calculate_deltaE(target_srgb, fitted_srgb)
     
        except:
            return 1000
    else:
        return fitted_srgb
  
        
def simulate_display_color_values(icc_profiles, original_color_values_luv, return_dict=None, procnum=None, plot=True):
    if plot == True:
        import matplotlib.pyplot as plt
        import os
        import datetime
        time = datetime.datetime.now()
        target_dir = 'fig_'+str(time.year)+str(time.month)+str(time.day)+'_'+str(time.hour)+str(time.minute)+str(time.second)
        if not os.path.isdir(target_dir):
            os.mkdir(target_dir)
    
    if return_dict == None:
        multiproc = False
    else:
        multiproc = True
    
    # ColorChecker values for deltaE2000 estimation
    from colormath.color_objects import sRGBColor
    from colormath.color_conversions import convert_color
    colorchecker_hex_values = ['c29682','f3f3f3','d67e2c','383d96',
                  '627a9d','c8c8c8','505ba6','469449',
                  '576c43','a0a0a0','c15a63','af363c',
                  '8580b1','7a7a7a','5e3c6c','e7c71f',
                  '67bdaa','555555','9dbc40','bb5695',
                  '343434','e0a32e','0885a1']

    calibration_color_values_srgb = []
    for value in colorchecker_hex_values:
        sRGB1_full =  sRGBColor.new_from_rgb_hex(value)
        calibration_color_values_srgb.append(sRGB1_full.get_value_tuple())

    # add the original colors first
    original_color_values_cam, original_color_values_srgb = luv_convert_list(original_color_values_luv)
    simulated_display_color_values_list = [{'average_deltaE_modeled': 0,
                                            'average_deltaE_measured': 0,
                                            'average_deltaE_panel': 0,
                                            'average_deltaS': 0,
                                            'average_deltaE_panel': 0,
                                            'precision_deltaE': 0,
                                            'filename': None,
                                            'model': 'canonical_color_values',
                                            'luv': original_color_values_luv,
                                            'cam': original_color_values_cam,
                                            'srgb': original_color_values_srgb,
                                            'plot_rad': [3.5, 3.5, 3.5, 3.5,
                                                         3.5, 3.5, 3.5, 3.5,
                                                         3.5, 3.5, 3.5, 3.5,
                                                         3.5, 3.5, 3.5, 3.5 ],
                                            's_inverted': False}]
    
    # model the expected representation for each icc profile
    for profile in icc_profiles:
        new_color_values_luv = []
        new_color_values_cam = []
        new_color_values_srgb = []
        simulation_deltaE_values = []
        inversecalib_deltaE_values = []
        deltaS_panel_list = []
        deltaS_calib_list = []
        new_colors_plot_rad = []
        fit_precision_deltaE_values = []
        fit_valid = False
        
        for target_srgb in original_color_values_srgb:
            res = minimize(find_srgb, target_srgb, method="Nelder-Mead", args=(profile, target_srgb), bounds = [(0,1),(0,1),(0,1)])
            fitted_srgb = res['x']
            fitted_cam = cspace_convert(fitted_srgb, 'sRGB1', 'CAM02-UCS')
            fitted_luv = cam_convert(fitted_cam)[0]
            fitted_srgb, alert = clip_srgb_alert(fitted_srgb)
          #  if alert:
          #      print('sRGB clipped')
            
            backtest_srgb = find_srgb(fitted_srgb, profile)
            new_color_values_luv.append(fitted_luv)
            new_color_values_cam.append(fitted_cam)
            new_color_values_srgb.append(fitted_srgb)
            new_colors_plot_rad.append(find_plot_radius_luv(fitted_luv,2,1,0.2))
            fit_precision_deltaE = calculate_deltaE(backtest_srgb, target_srgb)
            fit_precision_deltaE_values.append(fit_precision_deltaE)
            deltaS_panel_list.append(calculate_deltaS(target_srgb, fitted_srgb))
            
            # to estimate the deltaE2000 values, a CIELab-based calculation is needed
            deltaE2000_mode = True
            if deltaE2000_mode:
                target_Lab = cspace_convert(target_srgb, 'sRGB1', 'CIELab')
                fitted_Lab  = cspace_convert(fitted_srgb, 'sRGB1', 'CIELab')
                deltaE2000 = round(calculate_deltaE2000({'L': target_Lab[0], 'a': target_Lab[1], 'b': target_Lab[2]},
                                                        {'L': fitted_Lab[0], 'a': fitted_Lab[1], 'b': fitted_Lab[2]}),2)
                simulation_deltaE_values.append(deltaE2000)
            else:
                simulation_deltaE_values.append(calculate_deltaE(fitted_srgb, target_srgb))
        
        inversecalib_sRGB = []
        calib_xyY_target = []
        calib_xyY_fitted = []
        for target_srgb in calibration_color_values_srgb:
            res = minimize(find_srgb, target_srgb, method="Nelder-Mead", args=(profile, target_srgb), bounds = [(0,1),(0,1),(0,1)])
            fitted_srgb = res['x']
            fitted_srgb, alert = clip_srgb_alert(fitted_srgb)
            
            backtest_srgb = find_srgb(fitted_srgb, profile)
            fit_precision_deltaE = calculate_deltaE(backtest_srgb, target_srgb)
            fit_precision_deltaE_values.append(fit_precision_deltaE)
            inversecalib_sRGB.append(fitted_srgb)
            calib_xyY_target.append(cspace_convert(target_srgb, "sRGB1", "xyY1"))
            calib_xyY_fitted.append(cspace_convert(fitted_srgb, "sRGB1", "xyY1"))
            deltaS_calib_list.append(calculate_deltaS(target_srgb, fitted_srgb))
            
            # to estimate the deltaE2000 values, a CIELab-based calculation is needed
            if deltaE2000_mode:
                target_Lab = cspace_convert(target_srgb, 'sRGB1', 'CIELab')
                fitted_Lab  = cspace_convert(fitted_srgb, 'sRGB1', 'CIELab')
                deltaE2000 = round(calculate_deltaE2000({'L': target_Lab[0], 'a': target_Lab[1], 'b': target_Lab[2]},
                                                        {'L': fitted_Lab[0], 'a': fitted_Lab[1], 'b': fitted_Lab[2]}),2)
                inversecalib_deltaE_values.append(deltaE2000)
            else:
                inversecalib_deltaE_values.append(calculate_deltaE(fitted_srgb, target_srgb))
        
        # calculate average deltaS values
        sum_deltaS = 0
        for deltaS in deltaS_calib_list:
            sum_deltaS = sum_deltaS + deltaS
        average_deltaS = round(sum_deltaS / len(deltaS_calib_list),2)
        
        
        sum_deltaS = 0
        for deltaS in deltaS_panel_list:
            sum_deltaS = sum_deltaS + deltaS
        average_deltaS_panel = round(sum_deltaS / len(deltaS_calib_list),2)
        
        sum_deltaE = 0
        precision_sum_deltaE = 0
        for deltaE in fit_precision_deltaE_values:
            precision_sum_deltaE = precision_sum_deltaE + deltaE
        average_precision_deltaE = round(precision_sum_deltaE / len(fit_precision_deltaE_values),2)
            
        for deltaE in simulation_deltaE_values:
            sum_deltaE = sum_deltaE + deltaE
        average_deltaE = round(sum_deltaE / len(simulation_deltaE_values),2)
        
        sum_deltaE = 0
        for deltaE in inversecalib_deltaE_values:
            sum_deltaE = sum_deltaE + deltaE
        inverse_average_deltaE = round(sum_deltaE / len(inversecalib_deltaE_values),2)
        
        # validate fit and add to the rest or discard the output
        if average_precision_deltaE <= 1:
            if profile['average_deltaE']-0.5*profile['average_deltaE'] < inverse_average_deltaE < profile['average_deltaE']+0.5*profile['average_deltaE']:
                new_entry = {  'average_deltaE_modeled': inverse_average_deltaE,
                               'average_deltaE_measured': profile['average_deltaE'],
                               'average_deltaE_panel': average_deltaE,
                               'average_deltaS_panel': average_deltaS_panel,
                               'average_deltaS': average_deltaS,
                               'precision_deltaE': average_precision_deltaE,
                               'filename': profile['Filename'],
                               'model': profile['Model'],
                               'luv': new_color_values_luv,
                               'cam': new_color_values_cam,
                               'srgb': new_color_values_srgb,
                               'plot_rad': new_colors_plot_rad,
                               's_inverted': False}
                
                simulated_display_color_values_list.append(new_entry)
                
                # create a deltaS-inverted version as well
                s_inverted_color_values_srgb = []
                s_inverted_color_values_luv = []
                s_inverted_color_values_cam = []
                s_inverted_deltaE_values = []
                
                for i in range(len(new_color_values_srgb)):
                    s_inverted_srgb = invert_deltaS(original_color_values_srgb[i], new_color_values_srgb[i])
                    s_inverted_color_values_srgb.append(s_inverted_srgb)
                    s_inverted_cam, s_inverted_luv = srgb_convert(s_inverted_srgb)
                    s_inverted_color_values_luv.append(s_inverted_luv)
                    s_inverted_color_values_cam.append(s_inverted_cam)
                    
                for i in range(len(calibration_color_values_srgb)):
                    s_inverted_calibration_color = invert_deltaS(calibration_color_values_srgb[i], inversecalib_sRGB[i])
                    s_inverted_deltaE = calculate_deltaE(calibration_color_values_srgb[i], s_inverted_calibration_color)
                    s_inverted_deltaE_values.append(s_inverted_deltaE)
                    
                sum_deltaE = 0
                for deltaE in s_inverted_deltaE_values:
                    sum_deltaE = sum_deltaE + deltaE
                s_inverted_average_deltaE = round(sum_deltaE / len(s_inverted_deltaE_values),2)
                
                new_entry = {  'average_deltaE_modeled': s_inverted_average_deltaE,
                               'average_deltaE_measured': None,
                               'average_deltaE_panel': None,
                               'average_deltaS_panel': -average_deltaS_panel,
                               'average_deltaS': -average_deltaS,
                               'precision_deltaE': average_precision_deltaE,
                               'filename': profile['Filename'],
                               'model': profile['Model'],
                               'luv': s_inverted_color_values_luv,
                               'cam': s_inverted_color_values_cam,
                               'srgb': s_inverted_color_values_srgb,
                               'plot_rad': new_colors_plot_rad,
                               's_inverted': True }

                simulated_display_color_values_list.append(new_entry)                    
                print(profile['Filename'])
                print('deltaE: ', inverse_average_deltaE)
                print('deltaEi:', s_inverted_average_deltaE)
                print('deltaS: ', average_deltaS)
                print('precision:', average_precision_deltaE)                
                print('OK')
                plot = True
                if plot:
                    fig, ax = plt.subplots()
                    ax.set_xlim(-55,55)
                    ax.set_ylim(-95,60)
                    #ax.text(0,55,profile['Model'], horizontalalignment='center', verticalalignment='center', font = 'monospace')
                    ax.text(-53,49,'measured aver deltaE: '+str(profile['average_deltaE'])+'; calculated aver deltaE: '+str(round(average_deltaE,2)), horizontalalignment='left', verticalalignment='center', font = 'monospace')
                    for i in range(len(new_color_values_luv)):
                        ax.add_patch(plt.Circle((new_color_values_luv[i][0],new_color_values_luv[i][1]), new_colors_plot_rad[i], color=new_color_values_srgb[i]))
                    for i in range(len(original_color_values_luv)):    
                        ax.plot([original_color_values_luv[i][0],new_color_values_luv[i][0]],[original_color_values_luv[i][1], new_color_values_luv[i][1]],color='black')
                    for i in range(len(new_color_values_luv)):    
                        ax.add_patch(plt.Circle((original_color_values_luv[i][0],original_color_values_luv[i][1]), 0.5, color='black'))
                        ax.text(i*6.7-49.5,-74, i, horizontalalignment='center', verticalalignment='center', font = 'monospace')
                        ax.add_patch(plt.Circle((i*6.7-49.5,-80), 3.2, color=original_color_values_srgb[i]))
                        ax.add_patch(plt.Circle((i*6.7-49.5,-88), 3.2, color=new_color_values_srgb[i]))          
                    plt.savefig(target_dir+'/'+profile['Filename']+'.png', dpi=500)
                    plt.close()
                    
                    
                    fig, ax = plt.subplots()
                    ax.set_xlim(-0.5,1.15)
                    ax.set_ylim(-0.15,0.75)
                    ax.set_xlim(0,0.7)
                    ax.set_ylim(0,0.7)
                    #ax.text(-0.45,0.7,profile['Filename']+' ('+profile['Model']+')', horizontalalignment='left', verticalalignment='center', font = 'monospace')
                    ax.text(0.35,0.6,'meas aver deltaE: '+str(profile['average_deltaE'])+'; calc aver deltaE: '+str(round(inverse_average_deltaE,2)), horizontalalignment='center', verticalalignment='center', font = 'monospace')
                    for i in range(len(calib_xyY_fitted)):
                        ax.add_patch(plt.Circle((calib_xyY_fitted[i][0],calib_xyY_fitted[i][1]), 0.025, color=inversecalib_sRGB[i]))
                    for i in range(len(calib_xyY_target)):    
                        ax.plot([calib_xyY_target[i][0],calib_xyY_fitted[i][0]],[calib_xyY_target[i][1], calib_xyY_fitted[i][1]],color='black')
                    #for i in range(len(calib_xyY_fitted)):    
                        #ax.add_patch(plt.Circle((calib_xyY_target[i][0],calib_xyY_target[i][1]), 0.005, color='black'))
                        #ax.text(i*0.07-0.45, 0, i, horizontalalignment='center', verticalalignment='center', font = 'monospace')
                        #ax.add_patch(plt.Circle((i*0.07-0.45,-0.05), 0.022, color=calibration_color_values_srgb[i]))
                        #ax.add_patch(plt.Circle((i*0.07-0.45,-0.1), 0.022, color=inversecalib_sRGB[i]))          
                    plt.savefig(target_dir+'/'+profile['Filename']+'-calib.png', dpi=500)
                    plt.close()
        print()
        
    if multiproc:
         return_dict[procnum] = simulated_display_color_values_list
    else:
        return simulated_display_color_values_list


def test():
    d15_colors = [
                (-21.54,-38.39),
                (-23.26,-25.56), (-22.41,-15.53), (-23.11,-7.45),
                (-22.45,1.10), (-21.67,7.35), (-14.08,18.74),
                (-2.72,28.13), (14.84,31.13), (23.87,26.35),
                (31.28,14.76), (31.42,6.99), (29.79,0.10),
                (26.64,-9.38), (22.92,-18.65), (11.20,-24.61)]

    d15ds_colors = [
                (-4.77,-16.63),
                (-8.63,-14.65), (-12.08,-11.94), (-12.86,-6.74),
                (-12.26,-2.67), (-11.18,2.01),   (-7.02,9.12),
                (1.30,15.78),   (9.90,16.46),    (15.03,12.05),
                (15.48,2.56),   (14.76,-2.24),   (13.56,-5.04),
                (11.06,-9.17),  (8.95,-12.39),   (5.62,-15.20)]

    icc_profiles = pickle_load('_icc_profiles.pickle')
    
    color_values_list = parallelize(simulate_display_color_values,[icc_profiles, d15_colors],6)

from pickle_tools import *
from multiproc import *

if __name__ == '__main__':
    test_display_colors = test()
    pickle_dump('test_display_colors.pickle', test_display_colors)