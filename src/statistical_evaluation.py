# TLDR
# the responses of the displays were measured and saved to icm files
# these icm files were used to recreate the responses of the displays by fitting ICC's RGB -> XYZ algorithm to each calibration and D15(DS) color
# the actually measured responses are NOT the same as the ones that were modeled this way
# however, the EXTENT OF CHANGE in each color both in the calibration color and in the panel colors are the same
# therefore, if we know the real calibration responses and their deltaE, we can expect similar relative responses to the panel values as well
# the measured and modeled deltaE's for calibration colors show a very strong correlation, so we will use their linear fit
#         to extrapolate the otherwise underestimated calibration deltaE values
# aaand it's done: deltaE = 3 gives ~90% PPV, close enough for screening purposes
# also, NPV is always ~100%, and accuracy is always above 90% -- very similar to what we expect from physical D15 panel tests
from pickle_tools import *
import os
if not os.path.isdir('stats-figs'):
    os.mkdir('stats-figs')

def calculate_diagnostic_test(variable):
    try:
        TP = variable['tp']
        TN = variable['tn']
        FP = variable['fp']
        FN = variable['fn']
        try:
            sensitivity = round(TP / (TP + FN),3)
        except ZeroDivisionError:
            sensitivity = 1
        try:
            specificity = round(TN / (TN + FP),3)
        except ZeroDivisionError:
            specificity = 1
        try:
            accuracy = round((TP + TN) / (TP + TN + FP + FN),3)
        except ZeroDivisionError:
            accuracy = 1
        try:
            PPV = round(TP / (TP + FP),3)
        except ZeroDivisionError:
            PPV = 1
        try:
            NPV = round(TN / (TN + FN),3)
        except ZeroDivisionError:
            NPV = 1
        try:
            PLR = round(sensitivity / (1 - specificity),3)
        except ZeroDivisionError:
            PLR = 1000
        try:
            NLR = round((1 - sensitivity) / specificity,3)
        except ZeroDivisionError:
            NLR = 1000
    except UnicodeDecodeError:
        return None
    return {'Sensitivity': sensitivity, 'Specificity': specificity, 'Accuracy': accuracy, 'PPV': PPV, 'NPV': NPV, 'plr': PLR, 'nlr': NLR}

def diagnostic_test_statistic(accuracy_data, display_info, panel_shortname):
    # compute diagnostic accuracy for each simulated display
    
    displays = []
    for entry in display_info:
        skip = False
        new_display =  {'vyngris': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'vyngris_protan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'vyngris_deutan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'vyngris_tritan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'foutch': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'foutch_protan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'foutch_deutan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0},
                    'foutch_tritan': {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0}}
        for var in  ['filename', 's_inverted',
                     'average_deltaE_modeled','average_deltaE_measured','average_deltaE_panel',
                     'average_deltaS', 'average_deltaS_panel']:
            if var in entry.keys():
                new_display[var] = entry[var]
            else:
                skip = True
                
            if new_display not in displays and new_display['filename'] != ['SDC4179'] and not skip:
                displays.append(new_display)
    
    for display in displays:
        if 'average_deltaS' not in display.keys():
            displays.remove(display)
            
    print('DISPLAYS', len(displays))
    
    #for display in displays:
    #    print(display['filename'], display['average_deltaE_modeled'], display['average_deltaS'])
    
    
    for entry in accuracy_data:
        for display in displays:
            if display['filename'] == entry['filename']:
                for dg in ['vyngris', 'vyngris_protan', 'vyngris_deutan', 'vyngris_tritan', 'foutch', 'foutch_protan', 'foutch_deutan', 'foutch_tritan']:
                    if entry[dg] in display[dg]:
                        display[dg][entry[dg]] += 1
    
    
    
    import matplotlib.pyplot as plt
    # plot for measured and calculated display dE values for calibration colors
    def linear_fit(x):
        return 1.415*x - 0.110
    
    fig, ax = plt.subplots()
    ax.set_xlabel('modeled average display deltaE [dE2000 values]')
    ax.set_ylabel('average dsiplay deltaC [difference in CIECAM02 JCh chroma values]')
    #ax.set_xlim(0,5)
    x_axis = 'average_deltaE_modeled'
    y_axis = 'average_deltaS'
    title = 'modeled dS vs modeled dE'
    filename = 'modeled_dS_v_modeled_dE'
    ax.set_title(title)
    x = []
    y = []
    y_inverted = []
    x_inverted = []
    for display in displays:
        if display['s_inverted']:
            x_inverted.append(linear_fit(display[x_axis]))
            y_inverted.append(linear_fit(display[y_axis]))
        else:
            x.append(display[x_axis])
            y.append(display[y_axis])
    ax.scatter(x, y, color="#a29bfe")
    ax.scatter(x_inverted, y_inverted, color="#ff7675")
    ax.legend(('Profiled colors', 'Profiled colors with inverted dS'))
    #plt.show()
    plt.savefig('stats-figs/'+filename+'.png', dpi=500)
    plt.close()
    
    #Bland-Altman for measured vs modeled
    import matplotlib.pyplot as plt
    import numpy as np
    import pdb
    from numpy.random import random
    
    # diagnostic test plots
    for x_axis in ['average_deltaE_modeled', 'average_deltaS']:
        for var in ['Accuracy', 'Sensitivity', 'Specificity', 'NPV', 'PPV']:
            for dg in ['', '_protan', '_deutan', '_tritan']:
                fig, ax = plt.subplots()
                plt.locator_params(axis='x', nbins=5)
                if x_axis == 'average_deltaS':
                    ax.set_xlim(-12.5,12.5)
                    ax.set_xlabel('deltaC (CIECAM02 JCh chroma difference)')
                else:
                    ax.set_xlim(0,7)
                    ax.set_xlabel('deltaE (CAM02-UCS)')
                title = panel_shortname+' '+var+dg
                filename = panel_shortname+'_'+var+dg
                ax.set_title(title)
                x = []
                vyngris_y = []
                foutch_y = []
                x_i = []
                vyngris_y_i = []
                foutch_y_i = []
                for display in displays:    
                    try:
                        vyngris = calculate_diagnostic_test(display['vyngris'+dg])
                        foutch = calculate_diagnostic_test(display['foutch'+dg])                        
                        legend = ('FSL', 'VKS', 'FSL (S-inverted)', 'VKS (S-inverted)')
                        if x_axis == 'average_deltaS':
                            if display['s_inverted']:
                                vyngris_y_i.append(vyngris[var])
                                foutch_y_i.append(foutch[var])
                                x_i.append(display[x_axis])
                            else:
                                vyngris_y.append(vyngris[var])
                                foutch_y.append(foutch[var])
                                x.append(display[x_axis])
                        
                        else:
                            legend = ('FSL (dS >= 0)', 'VKS (dS >= 0)', 'FSL (dS < 0)', 'VKS (dS < 0)')
                            if display['average_deltaS'] < 0:
                                vyngris_y_i.append(vyngris[var])
                                foutch_y_i.append(foutch[var])
                                x_i.append(linear_fit(display[x_axis]))
                            else:
                                vyngris_y.append(vyngris[var])
                                foutch_y.append(foutch[var])
                                x.append(linear_fit(display[x_axis]))

                    except UnicodeDecodeError:
                        pass
                ax.scatter(x, foutch_y, color="#079992", alpha=0.6)
                ax.scatter(x, vyngris_y, color="#0a3d62", alpha=0.6)
                ax.scatter(x_i, foutch_y_i, color="#55efc4", alpha=0.4)
                ax.scatter(x_i, vyngris_y_i, color="#74b9ff", alpha=0.4)
                ax.legend(legend)
                ax.plot((-12.5,12.5),(0.9,0.9), color = "#fa983a")
                plt.savefig('stats-figs/'+x_axis+'_'+filename+'.png', dpi=500)
                plt.close()

    
if __name__ == '__main__':
    from pickle_tools import *
    panel_shortname = 'D15'
    accuracy_data_filename = 'cvd_accuracy_d15.pickle'
    displays_filename = 'display_colors_d15.pickle'
    accuracy_data = pickle_load(accuracy_data_filename)
    display_info = pickle_load(displays_filename)
    
    diagnostic_test_statistic(accuracy_data, display_info, panel_shortname)