def cam_convert(cam_color, illuminant='D65', lightness = 80):
    from colormath.color_objects import LuvColor, XYZColor, sRGBColor, LabColor, xyYColor, HSLColor
    from colormath.color_conversions import convert_color
    from colorspacious import cspace_convert
    
    # cam to luv via xyY
    xyY = cspace_convert(cam_color, "CAM02-UCS", "xyY1")
    xyY_color = xyYColor(xyY[0], xyY[1], xyY[2], illuminant=illuminant)
    luv_color = convert_color(xyY_color, LuvColor)
    luv_u = round(luv_color.luv_u, 2)
    luv_v = round(luv_color.luv_v, 2)
    luv = (luv_u, luv_v)
    
    # cam to srgb
    srgb = cspace_convert(cam_color, "CAM02-UCS", "sRGB1")
    
    return luv, srgb

def luv_convert(luv_color, illuminant='D65', lightness = 80):
    from colormath.color_objects import LuvColor, XYZColor, sRGBColor, LabColor, xyYColor
    from colormath.color_conversions import convert_color
    from colorspacious import cspace_convert
    
    L = lightness
    luv = LuvColor(L, luv_color[0], luv_color[1], illuminant=illuminant)
    xyY = convert_color(luv, xyYColor)
     
    # cam: Jp, ap, bp
    cam = cspace_convert([xyY.xyy_x,xyY.xyy_y,xyY.xyy_Y], "xyY1", "CAM02-UCS")
    srgb_raw = cspace_convert([xyY.xyy_x,xyY.xyy_y,xyY.xyy_Y], "xyY1", "sRGB1")
    
    srgb = []
    for color in srgb_raw:
        if color > 1:
            srgb.append(1)
        else:
            srgb.append(color)
    return cam, srgb

def srgb_convert(srgb_color, illuminant='D65', lightness = 80):
    from colormath.color_objects import LuvColor, XYZColor, sRGBColor, LabColor, xyYColor
    from colormath.color_conversions import convert_color
    from colorspacious import cspace_convert
    
    # srgb to luv via xyY
    xyY = cspace_convert(srgb_color, "sRGB1", "xyY1")
    xyY_color = xyYColor(xyY[0], xyY[1], xyY[2], illuminant=illuminant)
    luv_color = convert_color(xyY_color, LuvColor)
    luv_u = round(luv_color.luv_u, 2)
    luv_v = round(luv_color.luv_v, 2)
    luv = (luv_u, luv_v)
    
    # srgb to cam
    cam = cspace_convert(srgb_color, "sRGB1", "CAM02-UCS")
    
    return cam, luv
    
def luv_convert_list(luv_colors):
    cam_list = []
    srgb_list = []
    for color in luv_colors:
        cam, srgb = luv_convert(color)
        cam_list.append(cam)
        srgb_list.append(srgb)
    return cam_list, srgb_list

def srgb_convert_list(luv_colors):
    cam_list = []
    luv_list = []
    for color in luv_colors:
        cam, luv = srgb_convert(color)
        cam_list.append(cam)
        luv_list.append(luv)
    return cam_list, luv_list


def point_on_circle(center, degrees, radius):
    from math import cos, sin, pi, radians
    #center of circle, angle in degree and radius of circle
    angle = radians(degrees)
    x = round(center[0] + (radius * cos(angle)), 3)
    y = round(center[1] + (radius * sin(angle)), 3)
    return x,y

def caps_to_colorvalues(cap_order, colors):
    # colors[0] is the reference P cap
    # the cap numbers therefore correspond to the color indexes 1:1
    color_value_list = []
    for i in range(len(cap_order)):
        color_value_list.append(colors[cap_order[i]])
    return color_value_list

def calculate_centroid(points, colorspace='luv'):
    if colorspace == 'cam':
        x = [p[1] for p in points]
        y = [p[2] for p in points]
    else:
        x = [p[0] for p in points]
        y = [p[1] for p in points]
    centroid = (sum(x) / len(points), sum(y) / len(points))
    return centroid

def positional_angle(color, centroid):
    import math
    # define two line segments (in terms of their end-points)
    #            x0             y0              x1        y1
    segment1 = ((centroid[0], centroid[1]), (color[0],color[1]))
    segment2 = ((centroid[0], centroid[1]), (centroid[0], 30))

    # calculate the differences between the two end-points of each segment
    dx1 = segment1[1][0] - segment1[0][0]
    dy1 = segment1[1][1] - segment1[0][1]
    dx2 = segment2[1][0] - segment2[0][0]
    dy2 = segment2[1][1] - segment2[0][1]

    # calculate the angle between the two segments
    angle = math.atan2(dx1 * dy2 - dy1 * dx2, dx1 * dx2 + dy1 * dy2)

    # convert the angle to degrees and return it
    return math.degrees(angle)

def find_radius_luv(original_color_luv, deltaE_target, radius_start, radius_step):
    import colorspacious
    radius = radius_start
    deltaE = float('-inf')
    target_found = False
    plot_found = False # plot radius is radius for deltaE=2
    while True:
        for d in range(0,360,10):
            moved_color_luv = point_on_circle(original_color_luv, d, radius)
            original_color = luv_convert(original_color_luv)[0]
            moved_color = luv_convert(moved_color_luv)[0]
            deltaE_d = colorspacious.deltaE(original_color, moved_color, input_space='CAM02-UCS', uniform_space='CAM02-UCS')
            if deltaE_d > deltaE:
                deltaE = deltaE_d
                degree = d
            if deltaE >= deltaE_target and not target_found:
                target_found = True
                moved_color_luv_final = moved_color_luv
                moved_color_radius = round(radius,3)
                moved_color_degree_final= degree
                moved_color_deltaE = deltaE_d
            elif deltaE >= 2 and not plot_found:
                moved_color_radius_plot = radius
                plot_found = True

        if target_found and plot_found:
            # other possibly useful things to be printed/returned:
            # moved_color_luv_final, moved_color_degree_final, moved_color_deltaE
            return moved_color_radius, moved_color_radius_plot
        else:
            radius = radius + radius_step

def find_plot_radius_luv(original_color_luv, deltaE_target, radius_start, radius_step):
    import colorspacious
    radius = radius_start
    deltaE = float('-inf')
    original_color = luv_convert(original_color_luv)[0]
    target_found = False
    while True:
        for d in range(0,360,10):
            moved_color_luv = point_on_circle(original_color_luv, d, radius)
            moved_color = luv_convert(moved_color_luv)[0]
            deltaE_d = colorspacious.deltaE(original_color, moved_color, input_space='CAM02-UCS', uniform_space='CAM02-UCS')
            if deltaE_d > deltaE:
                deltaE = deltaE_d
                degree = d
            if deltaE >= deltaE_target and not target_found:
                target_found = True
                moved_color_radius = round(radius,3)
                
        if target_found:
            # other possibly useful things to be printed/returned:
            # moved_color_luv_final, moved_color_degree_final, moved_color_deltaE
            return moved_color_radius
        else:
            radius = radius + radius_step

def calculate_deltaS(col1, col2, input_colorspace='sRGB1'):
    from colorspacious import cspace_convert
    from colorspacious import deltaE as calculate_deltaE
    
    if input_colorspace != 'sRGB1':
        if input_colorspace == 'luv':
            col1 = luv_convert(col1)[1]
            col2 = luv_convert(col2)[1]
        else:
            try:
                col1 = cspace_convert(col1, input_colorspace, 'sRGB1')
                col2 = cspace_convert(col2, input_colorspace, 'sRGB1')
            except:
                print('sRGB1 conversion failed')
                return None

    original_deltaE = calculate_deltaE(col1, col2)
    
    JCh1 = cspace_convert(col1, "sRGB1", "JCh")
    JCh2 = cspace_convert(col2, "sRGB1", "JCh")

    C1 = JCh1[1]
    C2 = JCh2[1]
    return C2-C1


def invert_deltaS(col1, col2, match_percentage=False, input_colorspace='sRGB1'):
    from colorspacious import cspace_convert
    from colorspacious import deltaE as calculate_deltaE
    
    if input_colorspace != 'sRGB1':
        if input_colorspace == 'luv':
            col1 = luv_convert(col1)[1]
            col2 = luv_convert(col2)[1]
        else:
            try:
                col1 = cspace_convert(col1, input_colorspace, 'sRGB1')
                col2 = cspace_convert(col2, input_colorspace, 'sRGB1')
            except:
                print('sRGB1 conversion failed')
                return None

    JCh1 = cspace_convert(col1, "sRGB1", "JCh")
    JCh2 = cspace_convert(col2, "sRGB1", "JCh")
    J, C, h = JCh1[0], JCh1[1], JCh1[2]
    deltaC = JCh2[1]-JCh1[1]
    
    if deltaC > 0:
        new_JCh = (J, C-abs(deltaC), h)
    else:
        new_JCh = (J, C+abs(deltaC), h)

    return cspace_convert(new_JCh, "JCh", "sRGB1")

def clip_srgb(raw_srgb):
    clipped_srgb = []
    for i in raw_srgb:
        if 0 < i < 1:
            clipped_srgb.append(i)
        elif i > 1:
            clipped_srgb.append(1)
        else:
            clipped_srgb.append(0)
    return clipped_srgb

def clip_srgb_alert(raw_srgb):
    clipped_srgb = []
    alert = False
    for i in raw_srgb:
        if 0 < i < 1:
            clipped_srgb.append(i)
        elif i > 1:
            clipped_srgb.append(1)
            alert = True
        else:
            clipped_srgb.append(0)
            alert = True
    return clipped_srgb, alert

if __name__ == '__main__':
    from colorspacious import deltaE as calculate_deltaE
    