from os import listdir
import json
import numpy as np
from pickle_tools import *

def prepare_icc_profiles(directory):
    
    # import available display profiles in json format
    # https://github.com/mplough/icc_dump is used to convert originals to json
    icc_profiles = []
    for profile in listdir(directory):
        try:
            with open(directory+'/'+profile, "r") as read_file:
                data = json.load(read_file)
            data['Filename'] = profile[:-5]
            icc_profiles.append(data)
        except:
            print('Profile', profile, 'skipped due to parsing error.')

    # laptop data, in place for now:
    icc_metadata = []
    
    import os

    icc_metadata = []

    for file in os.listdir('./laptop-txt'):
        check = True
        deltaE=None; icm_url=None; icm_filename=None
        sRGB_percent=None; brightness=None; colorimeter=None
        
        infile = open('./laptop-txt/'+file, 'r')
        inlines = ''
        for line in infile.readlines():
            inlines = inlines+line
        splitlines=inlines.split(' ')
        
        for i in range(len(splitlines)):
            if 'Delta;E' in splitlines[i-1] and 'Color' in splitlines[i]:
                deltaE = float(splitlines[i+1])
                #print(deltaE)
            if '.icm' in splitlines[i] or '.icc' in splitlines[i]:
                icm_url = splitlines[i]
                icm_filename = icm_url.replace('href="', '')
                icm_filename = icm_filename.replace('"','')
                icm_filename = icm_filename.replace('uploads/tx_nbc2/','')
                icm_filename = icm_filename[:-4]
                icm_url = icm_url.replace('href="', 'notebookcheck.net/')
                icm_url = icm_url.replace('"','')
                #print(icm_url)
            if 'sRGB' in splitlines[i]:
                try:
                    sRGB_percent = splitlines[i][7:11]
                    sRGB_percent = sRGB_percent.replace('%','')
                    sRGB_percent = sRGB_percent.replace('&','')
                    sRGB_percent = float(sRGB_percent)
                    #print(sRGB_percent)
                except:
                    print('sRGB not found')
                    
            if '(Nits)' in splitlines[i]:
                brightness = splitlines[i+2][0:5]
                brightness = brightness.replace('&','')
                brightness = brightness.replace('n','')
                brightness = float(brightness)
                #print(brightness)
            if 'X-Rite' in splitlines[i]:
                colorimeter = splitlines[i] +' '+ splitlines[i+1] + ' ' + splitlines[i+2]
                for char in [':','(',')','<','>','/a', '/div', 'Maximum', '/td/tr/tableiDistribution']:
                    colorimeter = colorimeter.replace(char,'')
                #print(colorimeter)
            
        for var in [(deltaE, 'deltaE'), (icm_url,'icm_url'), (icm_filename, 'icm_filename'), (sRGB_percent,'sRGB_percent'), (brightness,'brightness'), (colorimeter, 'colorimeter')]:
            if var[0] == None:
                check = False
                if 'icm' not in var[1]:
                    pass
                    #print(file, 'failed with', var[1])
                break
        
        if check:
            new_entry = {'Filename': icm_filename,
                         'URL': icm_url,
                         'Model': file,
                         'average_deltaE': deltaE,
                         'sRGB_coverage': sRGB_percent,
                         'Brightness': brightness,
                         'Colorimeter': colorimeter}
            icc_metadata.append(new_entry) 
            

    for profile in icc_profiles:
        for info in icc_metadata:
            if profile['Filename'] == info['Filename']:
                profile['Model'] = info['Model']
                profile['average_deltaE'] = info['average_deltaE']
        
        # in this step we precalculate a few variables that will be needed for the color optimization,
        # keeping these values in memory instead of repeating the calculations
        # extract TRCs (only n=0 and n=1 cases are supported, linear interpolation will just fail for now)
        for TRC in ('RedTRC', 'GreenTRC', 'BlueTRC'):
            TRC_split = profile[TRC].split(" ")
            n = float.fromhex(TRC_split[11])
            new_key = TRC+'_gamma'
            if n == 1:
                TRC_value=float.fromhex(TRC_split[12]) + float.fromhex(TRC_split[13])/256
                profile[new_key] = TRC_value
            elif n == 0:
                profile[new_key] = 2.2
            else:
                print('Profile', profile['Filename'], 'skipped due to unsupported gamma profile.')
                icc_profiles.remove(profile)
        profile['TRC_n'] = n
        # generate the colorant matrix
        redColorant = profile['RedMatrixColumn'].split(' ')
        greenColorant = profile['GreenMatrixColumn'].split(' ')
        blueColorant = profile['BlueMatrixColumn'].split(' ')
        colorant_matrix = np.matrix([[float(redColorant[0]), float(greenColorant[0]), float(blueColorant[0])],
                                     [float(redColorant[1]), float(greenColorant[1]), float(blueColorant[1])],
                                     [float(redColorant[2]), float(greenColorant[2]), float(blueColorant[2])]])
        profile['colorantMatrix'] = colorant_matrix
        
    return icc_profiles

if __name__ == '__main__':
    from pickle_tools import *
    icc_profiles = prepare_icc_profiles('icc_json')
    pickle_dump('icc_profiles.pickle', icc_profiles)